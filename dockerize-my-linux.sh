#!/bin/bash

# Exit code:
# 0 = Success
# 1 = Unknown option
# 2 = Unexpected value
# 3 = Incomplete command
# 4 = Missing input file for load command
# 5 = Input file for load command does not exist

RESET="\e[0m"
RED='\033[0;31m'
BOLD="\e[1m"
UNDERLINE="\e[4m"
ARROW="\u21B3"

help() {
  if [ -z $1 ]; then
    TITLE="${BOLD}${UNDERLINE}dockerize-my-linux${RESET}"
    USAGE="usage_global"
  elif [ $1 == "generate" ]; then
    TITLE="${BOLD}${UNDERLINE}dockerize-my-linux generate${RESET}"
    USAGE="usage_generate"
  elif [ $1 == "load" ]; then
    TITLE="${BOLD}${UNDERLINE}dockerize-my-linux load${RESET}"
    USAGE="usage_load"
  fi

  echo ""
  echo -e "${TITLE}"
  echo ""
  echo -e "${BOLD}Description:${RESET}"
  echo "Dockerize-my-linux is a tool that will help you creates a docker image archive (.tar) from your current filesystem (root: /) and load it into your docker instance"
  echo ""
  echo -e "${BOLD}Warning:${RESET}"
  echo "This process requires a lot of free disk space (depending on your current filesystem usage)"
  echo ""
  echo -e "${BOLD}Requirements:${RESET}"
  echo "This script requires tar installed"
  echo ""
  echo -e "${BOLD}Usage:${RESET}"
  eval ${USAGE}
  echo ""
  echo -e "${BOLD}Exit code:${RESET}"
  echo -e "\t0 = Success"
  echo -e "\t1 = Unknown option"
  echo -e "\t2 = Unexpected value"
  echo -e "\t3 = Incomplete command"
  echo -e "\t4 = Missing input file for load command"
  echo -e "\t5 = Input file for load command does not exist"
}

usage_global() {
  echo "$0 [commands] [options]"
  echo -e "\t[commands]:"
  echo -e "\t\tgenerate: generate the docker image archive from your current filesystem (root: /)"
  echo -e "\t\tload: load a docker image archive into docker"
  echo -e "\tTo see specific command usage, use ${BOLD}$0 <command> -h${RESET} or ${BOLD}$0 <command> --help${RESET}"
  echo -e "\t[options]:"
  echo -e "\t\t-h|--help: print help"
}

usage_generate() {
  echo "$0 generate [options]"
  echo -e "\t[options]:"
  echo -e "\t\t-h|--help: print help"
  echo -e "\t\t-o|--output: Change the output file path (default=./dockerize-my-linux-output.tar)"
}

usage_load() {
  echo "$0 load <input-file> [options]"
  echo -e "\t<input-file>: The tar archive containing a docker image (generated with dockerize-my-linux)"
  echo -e "\t[options]:"
  echo -e "\t\t-h|--help: print help"
  echo -e "\t\t-o|--output-docker-image-name: the name of the docker image once loaded in your docker instance (default=dockerize-my-linux:latest)"
}

print_error() {
  echo -e "${RED}${BOLD}Error:${RESET} ${RED}${@}${RESET}"
}

print_title() {
  echo -e "${BOLD}${@}${RESET}"
}

generate() {
  OUTPUT_FILE_PATH="dockerize-my-linux-output.tar"

  POSITIONAL_ARGS=()

  while [[ $# -gt 0 ]]; do
    case $1 in
      -h|--help)
        help generate
        exit 0
        ;;
      -o|--output)
        OUTPUT_FILE_PATH="$2"
        shift 2
        ;;
      -*|--*)
        print_error "Unknown option $1"
        help generate
        exit 1
        ;;
      *)
        print_error "Unexpected value $1"
        help generate
        exit 2
        ;;
    esac
  done

  # Create the docker image archive (.tar) from the current filesystem
  print_title "Creating archive"
  echo -e "${ARROW}Archive file path: ${OUTPUT_FILE_PATH}"
  sudo tar -cvpf ${OUTPUT_FILE_PATH} --exclude=proc --exclude=sys --exclude=dev/pts --exclude=${OUTPUT_FILE_PATH} /
}

load() {
  INPUT_FILE_PATH=
  OUTPUT_DOCKER_IMAGE_NAME="dockerize-my-linux:latest"

  POSITIONAL_ARGS=()

  while [[ $# -gt 0 ]]; do
    case $1 in
      -h|--help)
        help load
        exit 0
        ;;
      -o|--output-docker-image-name)
        OUTPUT_DOCKER_IMAGE_NAME="$2"
        shift 2
        ;;
      -*|--*)
        print_error "Unknown option $1"
        help generate
        exit 1
        ;;
      *)
        INPUT_FILE_PATH=$1
        shift
        ;;
    esac
  done

  if [ -z ${INPUT_FILE_PATH} ]; then
    print_error "Missing input file for load command"
    help load
    exit 4
  fi

  if [ ! -f ${INPUT_FILE_PATH} ]; then
    print_error "Input file for load command does not exist"
    help load
    exit 5
  fi

  # Load the docker image archive (.tar)
  print_title "Loading docker image"
  echo -e "${ARROW}Docker image archive file path: ${INPUT_FILE_PATH}"
  cat ${INPUT_FILE_PATH} | docker import - ${OUTPUT_DOCKER_IMAGE_NAME}
}

# Parse subcommand
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      help
      exit 0
      ;;
    generate)
      shift
      generate $@
      exit 0
      ;;
    load)
      shift
      load $@
      exit 0
      ;;
    -*|--*)
      print_error "Unknown option $1"
      help
      exit 1
      ;;
    *)
      print_error "Unexpected value $1"
      help
      exit 2
      ;;
  esac
done

print_error "Incomplete command"
help
exit 3
