# DOCKERIZE-MY-LINUX

## Presentation

Dockerize-my-linux is a tool that will help you creates a docker image archive (.tar) from your current filesystem (root: /) and load it into your docker instance.

Dockerize-my-linux is working with **any linux distribution** (debian, fedora, ubuntu, centos...) and is really **easy to set up and use**.
  
Warning: This process requires a lot of free disk space (depending on your current disk usage)

## Installation

You can use dockerize-my-linux without installation, as it is only made of bash scripts.

However, dockerize-my-linux requires some dependencies:

- tar (dockerize-my-linux generate only)

- docker (deockerize-my-linux load only)

To get the project and all of its bash scripts, just clone the repository:

```git clone https://gitlab.com/zeusxduff/dockerize-my-linux.git```

## Usage

### General

```
./dockerize-my-linux [commands] [options]
	[commands]:
		generate: generate the docker image archive from your current filesystem (root: /)
		load: load a docker image archive into docker
	To see specific command usage, use ./dockerize-my-linux <command> -h or ./dockerize-my-linux <command> --help
	[options]:
		-h|--help: print help
```

### Generate

```
./dockerize-my-linux generate [options]
	[options]:
		-h|--help: print help
		-o|--output: Change the output file path (default=./dockerize-my-linux-output.tar)
```

### Load

```
./dockerize-my-linux load <input-file> [options]
	<input-file>: The tar archive containing a docker image (generated with dockerize-my-linux)
	[options]:
		-h|--help: print help
		-o|--output-docker-image-name: the name of the docker image once loaded in your docker instance (default=dockerize-my-linux:latest)
```

## Exit code:

```
0 = Success
1 = Unknown option
2 = Unexpected value
3 = Incomplete command
4 = Missing input file for load command
5 = Input file for load command does not exist
```
